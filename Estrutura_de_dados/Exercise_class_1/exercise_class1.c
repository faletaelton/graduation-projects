#include <stdio.h>    

// static int size_of_arrays = 5;
#define size_of_arrays 5

struct Range
{
    int maximuns[size_of_arrays];
    int minimuns[size_of_arrays];
};


int main(int argc, char const *argv[])
{
    struct Range range = {{20,30,40,50,60}, {11,22,35,44,52}};
    
    int index = 0;
    for (index = 0; index < size_of_arrays; ++index)
    {
        printf("%d ", range.maximuns[index] - range.minimuns[index]);
    }

    return 0;
}
