import java.util.Scanner;

public class Question9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the distance in km:");

        float distance = scanner.nextFloat();

        System.out.println("Enter the amount if liters consumed:");

        float liters = scanner.nextFloat();

        System.out.println(String.format("Your can is making %f per liter", distance/liters));  

        scanner.close();
        
    }
}
