import java.util.Random;

public class Question40 {
    public static void main(String[] args) {
        double random = new Random().nextDouble();
        int number = (int) (random * 28) + 3;
        int result = 1;
        for(int index = 1; index <=number; index++) {
            result = result * index;
        }

        System.out.println(String.format("number: %d, result: %d", number, number)); 
    }
}
