import java.util.Scanner;

public class Question52 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        double antecessor = 0;
        int outOfOrder = 0;


        while (keepAsking) {
            System.out.println("Number: To stop insert -1.");
            double number = scanner.nextDouble();
            if (number == -1) {
                keepAsking = false;
                continue;
            }
            if (number < antecessor) {
                outOfOrder += 1;
                continue;
            }
            antecessor = number;
        }

        System.out.println(String.format("out of order: %d", outOfOrder)); 

        scanner.close();
    }
}
