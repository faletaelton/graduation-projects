import java.util.Scanner;

public class Question6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number");

        double result = scanner.nextDouble() * -1;

        System.out.println(String.format("Result of operation: %f5", result));  

        scanner.close();
    }
}
