import java.util.Scanner;

public class Question58 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first edge");
        double firstEdge = scanner.nextDouble();  

        System.out.println("Enter the second edge");
        double secondEdge = scanner.nextDouble(); 

        System.out.println("Enter the second edge");
        double thirdEdge = scanner.nextDouble(); 

        String message = "";

        if ((firstEdge < (secondEdge + thirdEdge)) &&
            (secondEdge < (firstEdge + thirdEdge)) &&
            (thirdEdge < (firstEdge + secondEdge))) {
                if (firstEdge == secondEdge && secondEdge == thirdEdge) {
                    message = "It's a equilateral triangle";
                } else if ((firstEdge == secondEdge) ||
                            (secondEdge == thirdEdge) ||
                            (firstEdge == thirdEdge)){
                    message = "It's a isoceles triangle";
                } else {
                    message = "It's a scalene triangle";
                }

        } else {
            message = "Those dimensions do not build a triangle";
        }

        System.out.println(
            String.format
            ("Result: %s", 
                message
            )
        ); 
        
        scanner.close();
    }
}
