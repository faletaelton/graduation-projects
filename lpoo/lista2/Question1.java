import java.util.Scanner;

public class Question1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a integer to get it summed by 10:");

        int number = scanner.nextInt();  
        
        System.out.println(String.format("The number incresed by 10 is: %d", number + 10));  

        scanner.close();
    }
}
