import java.util.Scanner;

public class Question29 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Age:");
        int age = scanner.nextInt();

        boolean majority = age >= 18;
        boolean civilAge = age >= 21;

        System.out.println("majority: " + majority + " civil age: " + civilAge); 
    
        scanner.close();
    }
}
