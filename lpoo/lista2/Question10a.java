import java.util.Scanner;

public class Question10a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the temperature in Fahrenheit:");

        float fahrenheit = scanner.nextFloat();

        float inCelsius = ((float) 5 /(float) 9) * (fahrenheit - (float) 32);

        System.out.println(String.format("The temperature in Celsius: %f, and in Fahrenheit: %f", inCelsius, fahrenheit));  

        scanner.close();
        
    }
}
