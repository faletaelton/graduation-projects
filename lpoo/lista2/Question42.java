import java.util.Random;

public class Question42 {
    public static void main(String[] args) {
        int counter = 0;

        int positives = 0;
        int negatives = 0;
        int zeros = 0;
    
        Random random = new Random();
        while (counter < 200) {
            int number = random.nextInt();

            if (number == 0) {
                zeros += 1;
            } else if(number > 0) {
                positives += 1;
            } else if(number < 0) {
                negatives += 1;
            }      
            
            counter += 1;
        }

        System.out.println(String.format("negatives: %d, zeros: %d, positives: %d", negatives, zeros, positives)); 
    }
}
