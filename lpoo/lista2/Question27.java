import java.util.Scanner;

public class Question27 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double result = 0;

        for(int index = 0; index < 4; index++){
            System.out.println("Enter an number");
            Double number = scanner.nextDouble();
            if (number % 2 == 0) {
                result = result + number;
            }
        }

        System.out.println(String.format("Result of operation: %.2f", result));  

        scanner.close();
    }
}
