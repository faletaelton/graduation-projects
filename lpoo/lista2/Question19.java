import java.util.Scanner;

// 19. Escreva um programa que pergunte a quantidade de km percorridos por 
// um carro alugado pelo usuário, assim como a quantidade de dias pelos 
// quais o carro foi alugado. Calcule o preço a pagar, 
// sabendo que o carro custa R$ 60 por dia e R$ 0,15 por km rodado

public class Question19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("distance:");
        double distance = scanner.nextDouble();

        System.out.println("days:");
        double days = scanner.nextDouble();

        double price = (days * 60) + (distance * 0.15);

        System.out.println(String.format("Price: %.2f", price)); 

        scanner.close();
    }
}
