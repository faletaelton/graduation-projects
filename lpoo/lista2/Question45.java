import java.util.ArrayList;
import java.util.Scanner;

public class Question45 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number:");
           
        int number = scanner.nextInt();

        ArrayList<Integer> divisors = new ArrayList<Integer>();

        for(int index = 1; index <= number; index++) {
            if (number % index == 0) {
                divisors.add(index);
            }
        }

        System.out.println(
            String.format(
                "%s", 
                divisors.size() == 2 ? "prime number" : "not prime number"
            )
        ); 
        
        scanner.close();
    }
}
