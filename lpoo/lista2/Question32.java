import java.util.Scanner;

public class Question32 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Birth year:");
        int birthYear = scanner.nextInt();

        System.out.println("Birth month:");
        int birthMonth = scanner.nextInt();

        System.out.println("Current year:");
        int currentYear = scanner.nextInt();

        System.out.println("Current month:");
        int currentMonth = scanner.nextInt();

        int age = currentYear - birthYear - (currentMonth < birthMonth ? 1 : 0);

        System.out.println(String.format("age: %d", age)); 
    
        scanner.close();
    }
}
