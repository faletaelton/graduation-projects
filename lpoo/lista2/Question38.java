import java.util.Scanner;

public class Question38 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Lower limit:");
           
        int lower = scanner.nextInt();

        System.out.println("Upper limit:");
           
        int upper = scanner.nextInt();

        for(int index = lower; index <=upper; index++) {
            System.out.println(String.format("number: %d", index)); 
        }

        scanner.close();
    }
}
