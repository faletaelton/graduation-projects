import java.util.Scanner;

public class Question10c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Speed in km/h:");

        float kilometersPerHour = scanner.nextFloat();

        float metersPerSecond = (float) (kilometersPerHour / 3.6);

        System.out.println(String.format("Km/h: %f, and in m/s: %f", kilometersPerHour, metersPerSecond)); 

        scanner.close();
    }
}
