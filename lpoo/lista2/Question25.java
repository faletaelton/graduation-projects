import java.util.Scanner;

public class Question25 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double max = 0;

        for(int index = 0; index < 2; index++){
            System.out.println("Enter an number");
            Double number = scanner.nextDouble();
            if (number > max) {
                max = number;
            }
        }

        System.out.println(String.format("Max: %.2f", max));  

        scanner.close();
    }
}
