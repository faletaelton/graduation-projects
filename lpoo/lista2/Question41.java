import java.util.ArrayList;

public class Question41 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        int index = 1;
        while (numbers.size() < 30) {
            int sevenMultiple = index * 7;
            if ((sevenMultiple % 2 == 0) && 
                (sevenMultiple % 3 == 0)) {
                numbers.add(sevenMultiple);
            }

            index += 1;
        }

        for(Integer element: numbers) {
            System.out.println(String.format("result: %d", element)); 
        }

    }
        
}
