import java.util.Scanner;

public class Question3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the integer");

        int number = scanner.nextInt();

        int result = number + (number % 3);
        
        System.out.println(String.format("Result of operation: %d", result));  

        scanner.close();
    }
}
