import java.util.Scanner;

public class Question7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] numbers = new double[5];

        for(int index = 0; index < 5; index++){
            System.out.println("Enter an number");
            Double number = scanner.nextDouble();
            numbers[index] = number;
        }

        double sum = 0;
        for (double i : numbers) {
            sum += i;
        }

        double average = sum/5;

        System.out.println(String.format("Result of operation: %f", average));  

        scanner.close();
    }
}
