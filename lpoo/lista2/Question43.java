import java.util.Scanner;

public class Question43 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write number:");
        String number = scanner.nextLine();
        for(char element: number.toCharArray()) {
            System.out.println(String.format("%s", element)); 

        }
        scanner.close();
    }
    
}
