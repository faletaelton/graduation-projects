import java.util.ArrayList;
import java.util.Scanner;

public class Question44 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number:");
           
        int number = scanner.nextInt();

        ArrayList<Integer> divisors = new ArrayList<Integer>();

        for(int index = 1; index <= number; index++) {
            if (number % index == 0) {
                divisors.add(index);
            }
        }

        for(int element: divisors) {
            System.out.println(String.format("divisor: %s", element)); 

        }
        scanner.close();
    }
}
