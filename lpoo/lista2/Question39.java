public class Question39 {
    public static void main(String[] args) {
        int result = 1;
        for(int index = 1; index <=7; index++) {
            result = result * index;
        }

        System.out.println(String.format("result: %d", result)); 
    }
}
