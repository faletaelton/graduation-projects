import java.util.Scanner;

public class Question30 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number:");
        int number = scanner.nextInt();

        String[] names = { 
        "zero", 
        "one", 
        "two",
        "three", 
        "four",
        "five", 
        "six", 
        "seven", 
        "eight", 
        "nine"
    };


        System.out.println(String.format("Number: %s", names[number]));  
    
        scanner.close();
    }
}
