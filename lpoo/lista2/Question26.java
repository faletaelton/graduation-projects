import java.util.Scanner;

public class Question26 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Year:");
        int year = scanner.nextInt();

        boolean divisibleBy400 = (year % 400 == 0);
        boolean divisibleBy4notBy100 = ((year % 4 == 0) && (year % 100 != 0));

        System.out.println("Leap year: " + (divisibleBy400 || divisibleBy4notBy100)); 
    
        scanner.close();
    }
}
