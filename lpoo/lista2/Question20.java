import java.util.Scanner;

// 20. Escreva um programa para calcular a redução do 
// tempo de vida de um fumante. Pergunte a quantidade de 
// cigarros fumados por dia e quantos anos ele já fumou. 
// Considere que um fumante perde 10 minutos de vida a cada cigarro,
//  calcule quantos dias de vida um fumante perderá. Exiba o total em dias.

public class Question20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("cigarretes per day:");
        int cigarretesPerDay = scanner.nextInt();

        System.out.println("Years smoking:");
        double yearsSmoking = scanner.nextInt();

        double daysLost = (((365 * yearsSmoking) * cigarretesPerDay) * 10) / 3600;

        System.out.println(String.format("Days lost: %f", daysLost)); 

        scanner.close();
    }
}
