import java.util.Scanner;

public class Question47 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("First number:");
           
        int first = scanner.nextInt();

        System.out.println("Second number:");
           
        int second = scanner.nextInt();

        int mdc = 1;
        for(int index = 1; index <= Math.min(first, second); index++) {
            if ((first % index == 0) && (second % index == 0)) {
                mdc = index;
            } 
        }

        System.out.println(String.format("%d",mdc));
        scanner.close();
    }
}
