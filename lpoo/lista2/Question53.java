import java.util.ArrayList;
import java.util.Scanner;

public class Question53 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        ArrayList<Integer> manAges = new ArrayList<Integer>();
        ArrayList<Integer> womanAges = new ArrayList<Integer>();

        while (keepAsking) {
            System.out.println("Gender: M/F");
            String gender = scanner.next();

            System.out.println("Age:");
            int age = scanner.nextInt();

            if (gender.compareTo("M") == 0) {
                manAges.add(age);
            } else {
                womanAges.add(age);
            }

            System.out.println("Keep adding? NO/YES");
            String answer = scanner.next();
            if (answer.toUpperCase().compareTo("NO") == 0) {
                keepAsking = false;
            }
        }

        double sumMan = 0;
        for(int element: manAges) { sumMan += element; }
        double averageMan = sumMan/manAges.size(); 

        double sumWoman = 0;
        for(int element: womanAges) { sumWoman += element; }
        double averageWoman = sumWoman/womanAges.size(); 


        System.out.println(
            String.format
            ("average man age: %.2f, amount of man: %d, average woman age: %.2f, amount of woman: %d", 
                averageMan, 
                manAges.size(), 
                averageWoman, 
                womanAges.size()
            )
        ); 

        scanner.close();
    }
}
