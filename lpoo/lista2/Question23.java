import java.util.Scanner;

public class Question23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write word:");
        String quote = scanner.nextLine();
        String inverted = "";
        for(int index = quote.length() - 1; index >= 0; index--) {
            inverted = inverted + quote.charAt(index);
        }
        Boolean isPalindrome = quote.compareTo(inverted) == 0;
        System.out.println(String.format("Is palindrome: %s", isPalindrome)); 
        scanner.close();
    }
}
