import java.util.Scanner;

public class Question4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] numbers = new double[4];


        for(int index = 0; index < 4; index++){
            System.out.println("Enter an number");
            Double number = scanner.nextDouble();
            numbers[index] = number;
        }

        double result = Math.pow(((numbers[0] + numbers[1])/(numbers[2] + numbers[3])), 2);  

        
        System.out.println(String.format("Result of operation: %f", result));  

        scanner.close();
    }
}
