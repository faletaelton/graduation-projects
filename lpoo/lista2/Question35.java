
public class Question35 {
    public static void main(String[] args) {
        int sum = 0;
        for(int index = 1; index <=100; index++) {
            sum += index;
        }
        double result = sum/100.0; 
        System.out.println(String.format("result: %.2f", result)); 
    }
}
