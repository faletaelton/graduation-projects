import java.util.Scanner;

public class Question51 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        int positives = 0;
        int negatives = 0;

        while (keepAsking) {
            System.out.println("Number: To stop insert 0.");
            double number = scanner.nextDouble();
            if (number == 0) {
                keepAsking = false;
            }

            if(number > 0) {
                positives += 1;
            } else if(number < 0) {
                negatives += 1;
            }   
        }

        System.out.println(String.format("negatives: %d, positives: %d", negatives, positives)); 

        scanner.close();
    }
}
