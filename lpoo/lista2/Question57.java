import java.util.Scanner;

public class Question57 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Polution index:");
        double index = scanner.nextInt();
       
        String level = "";
        if (index < 35) {
            level = "pleasant";
        } else if (index < 60) {
            level = "unpleasant";
        } else {
            level = "dangerous";
        }
        System.out.println(
            String.format
            ("Polution level: %s", 
                level
            )
        ); 

        scanner.close();
    }
}
