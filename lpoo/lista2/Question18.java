import java.util.Scanner;

public class Question18 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("distance:");
        double distance = scanner.nextDouble();

        System.out.println("speed:");
        double speed = scanner.nextDouble();

        double timeInHours = distance / speed;

        System.out.println(String.format("Time: %.2f", timeInHours)); 

        scanner.close();
    }
}
