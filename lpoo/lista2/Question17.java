import java.util.Scanner;

public class Question17 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("price:");
        double price = scanner.nextDouble();

        System.out.println("discount:");
        double discount = scanner.nextDouble();

        double newPrice = price - (discount/100 * price);

        System.out.println(String.format("New price: %.2f", newPrice)); 

        scanner.close();
    }



    // System.out.println("discount:");
    // double discount = scanner.nextDouble();
}
