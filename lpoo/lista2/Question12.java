import java.util.Scanner;

public class Question12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Fisrt side:");
        double first = scanner.nextDouble();

        System.out.println("Second side:");
        double second = scanner.nextDouble();

        System.out.println("Third size:");
        double third = scanner.nextDouble();

        double t = (first + second + third)/2;
        double argument = t * (t - first) * (t - second) * (t - third);
        double area = Math.pow(argument, (1.0/2.0));

        System.out.println(String.format("Area: %f", area)); 

        scanner.close();
    }
}