import java.util.Scanner;

public class Question14 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("a parameter:");
        double firstScore = scanner.nextDouble();

        System.out.println("b parameter:");
        double firstWeight = scanner.nextDouble();

        System.out.println("a parameter:");
        double secondScore = scanner.nextDouble();

        System.out.println("b parameter:");
        double secondWeight = scanner.nextDouble();

        double average = (firstScore * firstWeight) + (secondScore * secondWeight) / (firstWeight + secondWeight);

        System.out.println(String.format("Average: %f", average)); 

        scanner.close();
    }
}
