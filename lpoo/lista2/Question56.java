import java.util.Scanner;

public class Question56 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Work days:");
        double days = scanner.nextInt();

            
        double taxes = 0.08;
        double dayPrice = 20.0;

        double payment = (dayPrice * days) * (1.00 - taxes);
 
        System.out.println(
            String.format
            ("payment: %.2f", 
                payment
            )
        ); 

        scanner.close();
    }
}
