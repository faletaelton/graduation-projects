import java.util.Scanner;

public class Question10b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Rain in inches:");

        float inches = scanner.nextFloat();

        float milimeters = (float) (25.4 * inches);

        System.out.println(String.format("Inches: %f, and in milimeters: %f", inches, milimeters)); 

        scanner.close();
    }
}
