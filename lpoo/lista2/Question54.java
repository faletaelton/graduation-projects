import java.util.ArrayList;
import java.util.Scanner;

public class Question54 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        ArrayList<Integer> callsDurations = new ArrayList<Integer>();

        while (keepAsking) {
            System.out.println("Call duration:");
            int callDuration = scanner.nextInt();
            callsDurations.add(callDuration);

            System.out.println("Keep adding? NO/YES");
            String answer = scanner.next();
            if (answer.toUpperCase().compareTo("NO") == 0) {
                keepAsking = false;
            }
        }
        int credit = 100;

        for(int callDuration: callsDurations) { 
            credit -= 2;

            if (callDuration > 4) {
                int overFourMinutes = callDuration - 4;
                credit = credit - (overFourMinutes/4) - (overFourMinutes % 4 != 0 ? 1 : 0);
            }
        }
 
        System.out.println(
            String.format
            ("final credit: %d, bill: %.2f", 
                credit, 
                (credit < 0 ? credit * -0.17 : 0)
            )
        ); 

        scanner.close();
    }
}
