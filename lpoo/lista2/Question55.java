import java.util.ArrayList;
import java.util.Scanner;

public class Question55 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        ArrayList<Double> salaries = new ArrayList<Double>();

        while (keepAsking) {
            System.out.println("Salary:");
            double salary = scanner.nextDouble();
            salaries.add(salary);

            System.out.println("Keep adding? NO/YES");
            String answer = scanner.next();
            if (answer.toUpperCase().compareTo("NO") == 0) {
                keepAsking = false;
            }
        } 
        
        double totalDiscounts = 0;
        double totalSalaries = 0;
        double totalSalariesDiscounted = 0;

        for(double salary: salaries) { 
            totalSalaries += salary;
            if (salary > 1257.12 && salary <= 2512.08) {
                double discountedSalary = (salary * 0.85) - 188.57;
                totalSalariesDiscounted += discountedSalary;
                totalDiscounts += (salary - discountedSalary);
            } else if (salary > 2512.08) {
                double discountedSalary = (salary * 0.725) - 502.58;
                totalSalariesDiscounted += discountedSalary;
                totalDiscounts += (salary - discountedSalary);
            }
        }
 
        System.out.println(
            String.format
            ("discounts: %.2f, total salaries: %.2f, , total salaries discounted: %.2f", 
                totalDiscounts, 
                totalSalaries,
                totalSalariesDiscounted
            )
        ); 

        scanner.close();
    }
}
