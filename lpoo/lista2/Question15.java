import java.util.Scanner;

public class Question15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("hours:");
        int hours = scanner.nextInt();

        System.out.println("minutes:");
        int minutes = scanner.nextInt();

        System.out.println("seconds:");
        int seconds = scanner.nextInt();

        int inSeconds = (hours * 60 * 60) + (minutes * 60) + seconds;

        System.out.println(String.format("In seconds: %d", inSeconds)); 

        scanner.close();
    }
}
