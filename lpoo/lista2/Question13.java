import java.util.Scanner;

public class Question13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("a parameter:");
        double first = scanner.nextDouble();

        System.out.println("b parameter:");
        double second = scanner.nextDouble();

        double x = -1 * (second / first);

        System.out.println(String.format("Value of X: %f", x)); 

        scanner.close();
    }
}
