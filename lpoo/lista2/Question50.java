import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Question50 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        Map<String, Double> map = new HashMap<String, Double>();

        while (keepAsking) {
            System.out.println("Number:");
            double score = scanner.nextDouble();

            System.out.println("Name:");
            String name = scanner.next();

            map.put(name, score);

            System.out.println("Keep adding? NO/YES");
            String answer = scanner.next();
            if (answer.toUpperCase().compareTo("NO") == 0) {
                keepAsking = false;
            }
        }

        String maxKey = "";
        double max = 0;
        
        for(String key: map.keySet()) {
            if (map.get(key) > max) {
                max = map.get(key);
                maxKey = key;
            }
        }
        System.out.println(String.format("name: %s, max: %.2f", maxKey, max)); 

        scanner.close();
    }
}
