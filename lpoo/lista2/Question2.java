import java.util.Scanner;

public class Question2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first integer");

        double firstNumber = scanner.nextDouble();  

        System.out.println("Enter the second integer");

        double secondNumber = scanner.nextDouble();  

        double w = Math.pow(firstNumber - secondNumber, 2);
        
        System.out.println(String.format("Squared difference of %f and %f is %f", firstNumber, secondNumber, w));  

        scanner.close();
    }
}
