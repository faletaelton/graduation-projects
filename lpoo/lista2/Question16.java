import java.util.Scanner;

public class Question16 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("1 - hours:");
        int first_hours = scanner.nextInt();

        System.out.println("1 - minutes:");
        int first_minutes = scanner.nextInt();

        System.out.println("1 - seconds:");
        int first_seconds = scanner.nextInt();

        System.out.println("2 - hours:");
        int second_hours = scanner.nextInt();

        System.out.println("2 - minutes:");
        int second_minutes = scanner.nextInt();

        System.out.println("2 - seconds:");
        int second_seconds = scanner.nextInt();

        int first_inSeconds = (first_hours * 60 * 60) + (first_minutes * 60) + first_seconds;
        int second_inSeconds = (second_hours * 60 * 60) + (second_minutes * 60) + second_seconds;

        System.out.println(String.format("Diff in seconds: %d", Math.abs(first_inSeconds - second_inSeconds))); 

        scanner.close();
    }
}
