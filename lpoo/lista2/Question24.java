import java.util.Scanner;

public class Question24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("first:");
        double first = scanner.nextDouble();

        System.out.println("second:");
        double second = scanner.nextDouble();

        Double result = null;

        if (second != 0) {
            result = first/second;
        }

        if (result == null) {
            System.out.println("It was not possible to do the operation"); 
        } else {
            System.out.println(String.format("Result: %.2f", result)); 
        }

        scanner.close();
    }
}
