import java.util.ArrayList;

public class Question46 {
    public static void main(String[] args) {
        int number = 1;
        ArrayList<Integer> prime = new ArrayList<Integer>();

        while(prime.size() < 500) {
            ArrayList<Integer> divisors = new ArrayList<Integer>();

            for(int index = 1; index <= number; index++) {
                if (number % index == 0) {
                    divisors.add(index);
                }
            }

            if (divisors.size() == 2) {
                prime.add(number);
            }
            
            number += 1;
        }
        
        System.out.println(prime.toString());
    }
}
