import java.util.Scanner;

public class Question11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bigger base:");
        float biggerBase = scanner.nextFloat();

        System.out.println("Smaller base:");
        float smallerBase = scanner.nextFloat();

        System.out.println("High:");
        float high = scanner.nextFloat();

        float area = (biggerBase + smallerBase) * high/2;

        System.out.println(String.format("Area: %f", area)); 

        scanner.close();

    }
}