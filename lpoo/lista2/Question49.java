import java.util.ArrayList;
import java.util.Scanner;

public class Question49 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean keepAsking = true;
        ArrayList<Double> numbers = new ArrayList<Double>();

        while (keepAsking) {
            System.out.println("Number:");
            double number = scanner.nextDouble();
            numbers.add(number);

            System.out.println("Keep adding? NO/YES");
            String answer = scanner.next();
            if (answer.toUpperCase().compareTo("NO") == 0) {
                keepAsking = false;
            }
        }

        double sum = 0;
        double min = Integer.MAX_VALUE;
        double max = 0;
        
        for(double element: numbers) {
            sum += element;
            if (element > max) {
                max = element;
            }
            if (element < min) {
                min = element;
            }
        }
        double result = sum/numbers.size(); 
        System.out.println(String.format("average: %.2f, min:  %.2f, max:  %.2f", result, min, max)); 

        scanner.close();
    }
}
