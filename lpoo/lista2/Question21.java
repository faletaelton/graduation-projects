import java.util.Scanner;

public class Question21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write quote:");
        String quote = scanner.nextLine();
        String[] splitted = quote.split(" ");

        System.out.println(String.format("words: %d", splitted.length)); 
        scanner.close();
    }
}
