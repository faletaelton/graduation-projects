import java.util.Scanner;

public class Question31 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number:");
        int number = scanner.nextInt();
        String numberToString = Integer.toString(number);

        String[] zeroToNine = { 
            "zero", 
            "one", 
            "two",
            "three", 
            "four",
            "five", 
            "six", 
            "seven", 
            "eight", 
            "nine"
        };

        String[] elevenToNineten = { 
            "",
            "eleven", 
            "twelve", 
            "thirteen",
            "fourteen", 
            "fifhteen",
            "sixteen", 
            "seventeen", 
            "eighteen", 
            "nineteen", 
        };

        String[] tens = {
            "",
            "",
            "Twenty",
            "Thirty",
            "Forty",
            "Fifty",
            "Sixty",
            "Seventy",
            "Eighty",
            "Ninety"
        };  

        char[] digits = numberToString.toCharArray();
        int[] intDigits = new int[digits.length];
        for(int i = 0; i < digits.length; i++) {
            intDigits[i] = Character.getNumericValue(digits[i]);
        }

        String result = "";

        if (number < 10) {
            result += zeroToNine[number];
        } else if (number > 10 && number < 20) {
            result += elevenToNineten[number % 10];
        } else {
            result += tens[intDigits[0]];
            result += " " + zeroToNine[intDigits[1]];
        }

        System.out.println(String.format("Number: %s", result));  
    
        scanner.close();
    }
}