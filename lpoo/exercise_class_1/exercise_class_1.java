import java.util.ArrayList;
import java.util.Scanner;

class ExerciseClass1 {
    public static void main(String[] args) {
        // replaceVariables();
        // getAntecessor();
        // rectangleArea();
        // ageInDays();
        // elections();
        // salaryRaise();
        // carFinalPrice();
        sellerSalary();
    }

    static void replaceVariables() {
        int A = 10;
        int B = 20;
        int C = B;
        B = A;
        A = C;

        String output = String.format("A = %d and B = %d.", A, B);

        System.out.println(output); 
    }

    static void getAntecessor() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a integer to get its predecessor");

        String input = scanner.nextLine();  
        int number = Integer.parseInt(input);
        int predecessor = number -  1;
        
        System.out.println(String.format("The predecessor is: %d", predecessor));  

        scanner.close();
    }

    static void rectangleArea() {
        Scanner scanner = new Scanner(System.in);
        String[] dimensionNames = {"First", "Second"};
        ArrayList<Integer> dimensions = new ArrayList<Integer>();

        for (String element : dimensionNames) {
            System.out.println("Enter the " + element + " rect dimension");
            String input = scanner.nextLine();  
            Integer dimension = Integer.parseInt(input);
            dimensions.add(dimension);
        }
        
        Integer area = 1;

        for (Integer element : dimensions) {
            area = area * element;
        }

        System.out.println(String.format("The area is: %d", area));  

        scanner.close();
    }

    static void ageInDays() {
        Scanner scanner = new Scanner(System.in);
        String[] questions = {"How old are you?", 
                              "How many months since your last birthday?", 
                              "How many days?"};
        ArrayList<Integer> ages = new ArrayList<Integer>();

        for (String element : questions) {
            System.out.println(element);
            String input = scanner.nextLine();  
            Integer number = Integer.parseInt(input);
            ages.add(number);
        }
        
        Integer ageInDays = ages.get(0) * 365 + ages.get(1) * 30 + ages.get(2);

        System.out.println(String.format("The age in days is: %d", ageInDays));  

        scanner.close();
    }

    static void elections() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {new Question("How many voters?"),
                                new Question("How many valid votes?"), 
                                new Question("How many null votes?"),
                                new Question("How many blank votes?"),};

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float voters = questions[0].answer;
        Float valids = questions[1].answer;
        Float nulls = questions[2].answer;
        Float blanks = questions[3].answer;

        Float validsPercentage = valids/voters * 100;
        Float nullsPercentage = nulls/voters * 100;
        Float blanksPercentage = blanks/voters * 100;
        
        System.out.println(String.format("Total voters: %.0f", voters));  
        System.out.println(String.format("valid percentage: %.1f%%", validsPercentage));  
        System.out.println(String.format("null percentage: %.1f%%", nullsPercentage));  
        System.out.println(String.format("blank percentage: %.1f%%", blanksPercentage));  

        scanner.close();
    }

    static void salaryRaise() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {
            new Question("Salary?"),
            new Question("Raise percentage?"),
        };

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float salary = questions[0].answer;
        Float raisePercentage = (questions[1].answer.floatValue() + 100)/100;

        Float newSalary = salary * raisePercentage;
        
        System.out.println(String.format("New Salary: %.2f", newSalary));  

        scanner.close();
    }

    static void carFinalPrice() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {
            new Question("How much it cost to build?"),
        };

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float cost = questions[0].answer;
        Float distributionCost = (float) ((28.0 + 100.0)/100.0);
        Float fees = (float) ((45.0 + 100.0)/100.0);

        Float finalPrice = cost * distributionCost * fees;
        
        System.out.println(String.format("Final price: %.2f", finalPrice));  

        scanner.close();
    }

    static void sellerSalary() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {new Question("Base salary?"),
                                new Question("How many cars you sold?"), 
                                new Question("How much money selling those cars?"),
                                new Question("How much is the commission?"),};

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float baseSalary = questions[0].answer;
        Float numberOfCarsSold = questions[1].answer;
        Float soldAmount = questions[2].answer;
        Float comission = questions[3].answer;

        Float finalSalary = (float) (baseSalary + (numberOfCarsSold * comission) + (0.05 * soldAmount));

        System.out.println(String.format("Final Salary: %.2f", finalSalary));

        scanner.close();
    }
}