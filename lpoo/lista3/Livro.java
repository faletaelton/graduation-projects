class Livro {
    int numeroDePaginas;
    String genero;
    String titulo;
    String autor;
    Double preco;
    String editora;

    public Livro(int numeroDePaginas, String genero, String titulo, String autor, Double preco, String editora) {
        this.numeroDePaginas = numeroDePaginas;
        this.genero = genero;
        this.titulo = titulo;
        this.autor = autor;
        this.preco = preco;
        this.editora = editora;
    }
    
    @Override
    public String toString() {
        return "Livro [autor=" + autor + ", editora=" + editora + ", genero=" + genero + ", numeroDePaginas="
                + numeroDePaginas + ", preco=" + preco + ", titulo=" + titulo + "]";
    }


}