class Moradia {
    String rua;
    int numero;
    String dono;
    String bairro;
    String cidade;
    int numeroDeQuartos;
    Double areaDoTerreno;
    Double areaConstruida;

    public Moradia(String rua, int numero, String dono, String bairro, String cidade, int numeroDeQuartos,
            Double areaDoTerreno, Double areaConstruida) {
        this.rua = rua;
        this.numero = numero;
        this.dono = dono;
        this.bairro = bairro;
        this.cidade = cidade;
        this.numeroDeQuartos = numeroDeQuartos;
        this.areaDoTerreno = areaDoTerreno;
        this.areaConstruida = areaConstruida;
    }

    @Override
    public String toString() {
        return "Moradia [areaConstruida=" + areaConstruida + ", areaDoTerreno=" + areaDoTerreno + ", bairro=" + bairro
                + ", cidade=" + cidade + ", dono=" + dono + ", numero=" + numero + ", numeroDeQuartos="
                + numeroDeQuartos + ", rua=" + rua + "]";
    }
}