public class Principal {
    public static void main(String[] args) {
        System.out.println(new Aluno("Elton", "asdasd3123asd12").toString());
        System.out.println(new Animal("cachorro", 4).toString());
        System.out.println(new BombaDeCombustivel(3.1, 13, "Fulano").toString());
        System.out.println(new Calculadora(10, false).toString());
        System.out.println(new Carro(4, 4, "uno", "fiat", 66, 9.5, 15000.00).toString());
        System.out.println(new Cliente("Elton", "12312312312", 13345.34).toString());
        System.out.println(new ContaBancaria(123, 123, true).toString());
        System.out.println(new Contato("Fulano", "75999991111", "fulano@bol.com", "Rua dos bobos, numero 0", "fulaninho").toString());
        System.out.println(new Continente(30.0, 22.0, 40.0, 20.0, 40000000000.0, "America").toString());
        System.out.println(new Data(10, 04, 1994, true).toString());
        System.out.println(new Elevador(10.0, 4, 10, 0).toString());
        System.out.println(new Funcionario("Sicrano", 2, 2000.0, "TI").toString());
        System.out.println(new Ingresso(10.00, "asas livres", "bar da leila").toString());
        System.out.println(new JogadorDeFutebol("Tafa", "Atletico", 99, 25).toString());
        System.out.println(new Lampada(55, "Branca", "Osran").toString());
        System.out.println(new Livro(304, "Ficção", "Metaverso", "Fulano Tech", 34.00, "Abril").toString());
        System.out.println(new Moradia("Rua dos Bobos", 0, "Fulano", "Engraçada", "Nenhuma", 3, 100.0, 75.0).toString());
        System.out.println(new MovimentoUniforme(10.0, 3.0).toString());
        System.out.println(new Musica(300, "Pop 100", "rei da cacimbinha", "Pop 100").toString());
        System.out.println(new Pais("Belize","America", 3234134.00, 12312411, "Belize", "Belize").toString());
        System.out.println(new Passageiro("123123123123", "Fulano", "Rio Real", "Garanhuns").toString());
        // System.out.println(new .toString());
        // System.out.println(new .toString());
        // System.out.println(new .toString());
        // System.out.println(new .toString());
        // System.out.println(new .toString());
        // System.out.println(new .toString());
        

    }
}
