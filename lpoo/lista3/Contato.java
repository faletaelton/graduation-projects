class Contato {
    String nome;
    String telefone;
    String email;
    String endereço;
    String instagram;

    public Contato(String nome, String telefone, String email, String endereço, String instagram) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.endereço = endereço;
        this.instagram = instagram;
    }

    @Override
    public String toString() {
        return "Contato [email=" + email + ", endereço=" + endereço + ", instagram=" + instagram + ", nome=" + nome
                + ", telefone=" + telefone + "]";
    }
}