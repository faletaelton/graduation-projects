class Passageiro {
    String cpf;
    String nome;
    String partida;
    String chegada;
    
    public Passageiro(String cpf, String nome, String partida, String chegada) {
        this.cpf = cpf;
        this.nome = nome;
        this.partida = partida;
        this.chegada = chegada;
    }

    @Override
    public String toString() {
        return "Passageiro [chegada=" + chegada + ", cpf=" + cpf + ", nome=" + nome + ", partida=" + partida + "]";
    }
}