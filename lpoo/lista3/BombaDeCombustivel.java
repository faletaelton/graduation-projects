class BombaDeCombustivel {
    Double vazao;
    int numero;
    String operador;

    public BombaDeCombustivel(Double vazao, int numero, String operador) {
        this.vazao = vazao;
        this.numero = numero;
        this.operador = operador;
    }

    @Override
    public String toString() {
        return "BombaDeCombustivel [numero=" + numero + ", operador=" + operador + ", vazao=" + vazao + "]";
    }
}