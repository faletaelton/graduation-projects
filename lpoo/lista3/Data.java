class Data {
    int dia;
    int mes;
    int ano;
    boolean depoisDeCristo;
    
    public Data(int dia, int mes, int ano, boolean depoisDeCristo) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.depoisDeCristo = depoisDeCristo;
    }

    @Override
    public String toString() {
        return "Data [ano=" + ano + ", depoisDeCristo=" + depoisDeCristo + ", dia=" + dia + ", mes=" + mes + "]";
    }

}