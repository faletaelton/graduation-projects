public class Animal {
    String especie;
    int numeroDePes;
    
    public Animal(String especie, int numeroDePes) {
        this.especie = especie;
        this.numeroDePes = numeroDePes;
    }

    @Override
    public String toString() {
        return "Animal [numeroDePes=" + numeroDePes + ", especie=" + especie + "]";
    }
}