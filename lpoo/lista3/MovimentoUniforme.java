class MovimentoUniforme {
    Double espacoFinal;
    Double velocidade;
    Double tempo;

    public MovimentoUniforme(Double velocidade, Double tempo) {
        this.espacoFinal = velocidade * tempo;
        this.velocidade = velocidade;
        this.tempo = tempo;
    }

    @Override
    public String toString() {
        return "MovimentoUniforme [espacoFinal=" + espacoFinal + ", tempo=" + tempo + ", velocidade=" + velocidade
                + "]";
    }
}