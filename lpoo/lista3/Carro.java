public class Carro {
    int portas;
    int rodas;
    String nome;
    String marca;
    int potencia;
    Double torque;
    Double preco;
    
    public Carro(int portas, int rodas, String nome, String marca, int potencia, Double torque, Double preco) {
        this.portas = portas;
        this.rodas = rodas;
        this.nome = nome;
        this.marca = marca;
        this.potencia = potencia;
        this.torque = torque;
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Carro [marca=" + marca + ", nome=" + nome + ", portas=" + portas + ", potencia=" + potencia + ", preco="
                + preco + ", rodas=" + rodas + ", torque=" + torque + "]";
    }

}
