class Pais {
    String nome;
    String continente;
    Double area;
    int população;
    String capital;
    String lingua;

    public Pais(String nome, String continente, Double area, int população, String capital, String lingua) {
        this.nome = nome;
        this.continente = continente;
        this.area = area;
        this.população = população;
        this.capital = capital;
        this.lingua = lingua;
    }

    @Override
    public String toString() {
        return "Pais [area=" + area + ", capital=" + capital + ", continente=" + continente + ", lingua=" + lingua
                + ", nome=" + nome + ", população=" + população + "]";
    }
}