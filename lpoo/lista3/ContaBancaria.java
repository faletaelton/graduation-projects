class ContaBancaria {
    int idBanco;
    int idConta;
    Boolean poupanca;
    
    public ContaBancaria(int idBanco, int idConta, Boolean poupanca) {
        this.idBanco = idBanco;
        this.idConta = idConta;
        this.poupanca = poupanca;
    }

    @Override
    public String toString() {
        return "ContaBancaria [idBanco=" + idBanco + ", idConta=" + idConta + ", poupanca=" + poupanca + "]";
    }
}