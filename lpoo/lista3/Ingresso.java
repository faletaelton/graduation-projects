class Ingresso {
    Double preco;
    String show;
    String local;
    
    public Ingresso(Double preco, String show, String local) {
        this.preco = preco;
        this.show = show;
        this.local = local;
    }

    @Override
    public String toString() {
        return "Ingresso [local=" + local + ", preco=" + preco + ", show=" + show + "]";
    }
}