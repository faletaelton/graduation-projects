class Calculadora {
    int numeroDeOperacoes;
    Boolean cientifica;

    public Calculadora(int numeroDeOperacoes, Boolean cientifica) {
        this.numeroDeOperacoes = numeroDeOperacoes;
        this.cientifica = cientifica;
    }

    @Override
    public String toString() {
        return "Calculadora [cientifica=" + cientifica + ", numeroDeOperacoes=" + numeroDeOperacoes + "]";
    }
}