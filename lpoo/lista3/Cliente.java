class Cliente {
    String nome;
    String cpf;
    Double montanteComprado;
    
    public Cliente(String nome, String cpf, Double montanteComprado) {
        this.nome = nome;
        this.cpf = cpf;
        this.montanteComprado = montanteComprado;
    }

    @Override
    public String toString() {
        return "Cliente [cpf=" + cpf + ", montanteComprado=" + montanteComprado + ", nome=" + nome + "]";
    } 
}