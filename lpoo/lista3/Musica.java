class Musica {
    int duracao;
    String nome;
    String artista;
    String album;

    public Musica(int duracao, String nome, String artista, String album) {
        this.duracao = duracao;
        this.nome = nome;
        this.artista = artista;
        this.album = album;
    }

    @Override
    public String toString() {
        return "Musica [album=" + album + ", artista=" + artista + ", duracao=" + duracao + ", nome=" + nome + "]";
    }
}