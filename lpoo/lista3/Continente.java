class Continente {
    Double latitudeMaxima;
    Double latitudeMinima;
    Double longitudeMaxima;
    Double longitudeMinima;
    Double extensão;
    String nome;
    
    public Continente(Double latitudeMaxima, Double latitudeMinima, Double longitudeMaxima, Double longitudeMinima,
            Double extensão, String nome) {
        this.latitudeMaxima = latitudeMaxima;
        this.latitudeMinima = latitudeMinima;
        this.longitudeMaxima = longitudeMaxima;
        this.longitudeMinima = longitudeMinima;
        this.extensão = extensão;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Continente [extensão=" + extensão + ", latitudeMaxima=" + latitudeMaxima + ", latitudeMinima="
                + latitudeMinima + ", longitudeMaxima=" + longitudeMaxima + ", longitudeMinima=" + longitudeMinima
                + ", nome=" + nome + "]";
    }
}