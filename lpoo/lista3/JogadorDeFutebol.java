class JogadorDeFutebol {
    String nome;
    String time;
    int habilidade;
    int idade;
    
    public JogadorDeFutebol(String nome, String time, int habilidade, int idade) {
        this.nome = nome;
        this.time = time;
        this.habilidade = habilidade;
        this.idade = idade;
    }

    @Override
    public String toString() {
        return "JogadorDeFutebol [habilidade=" + habilidade + ", idade=" + idade + ", nome=" + nome + ", time=" + time
                + "]";
    }
}