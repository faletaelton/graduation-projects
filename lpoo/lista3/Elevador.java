class Elevador {
    Double velocidade;
    int andarAtual;
    int andarMaximo;
    int andarMinimo;

    public Elevador(Double velocidade, int andarAtual, int andarMaximo, int andarMinimo) {
        this.velocidade = velocidade;
        this.andarAtual = andarAtual;
        this.andarMaximo = andarMaximo;
        this.andarMinimo = andarMinimo;
    }

    @Override
    public String toString() {
        return "Elevador [andarAtual=" + andarAtual + ", andarMaximo=" + andarMaximo + ", andarMinimo=" + andarMinimo
                + ", velocidade=" + velocidade + "]";
    }    
}