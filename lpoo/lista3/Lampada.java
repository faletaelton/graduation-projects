class Lampada {
    int potencia;
    String cor;
    String fabricante;
    
    public Lampada(int potencia, String cor, String fabricante) {
        this.potencia = potencia;
        this.cor = cor;
        this.fabricante = fabricante;
    }

    @Override
    public String toString() {
        return "Lampada [cor=" + cor + ", fabricante=" + fabricante + ", potencia=" + potencia + "]";
    }
}