class Funcionario {
    String nome;
    int nivel;
    Double salario;
    String setor;

    public Funcionario(String nome, int nivel, Double salario, String setor) {
        this.nome = nome;
        this.nivel = nivel;
        this.salario = salario;
        this.setor = setor;
    }
    
    @Override
    public String toString() {
        return "Funcionario [nivel=" + nivel + ", nome=" + nome + ", salario=" + salario + ", setor=" + setor + "]";
    }
}