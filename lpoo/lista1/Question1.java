class Question1 {
    public static void main(String[] args) {
        replaceVariables();
    }

    static void replaceVariables() {
        int A = 10;
        int B = 20;
        int C = B;
        B = A;
        A = C;

        String output = String.format("A = %d and B = %d.", A, B);

        System.out.println(output); 
    }
}