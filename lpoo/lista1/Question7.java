import java.util.Scanner;

public class Question7 {
    public static void main(String[] args) {
        carFinalPrice();
    }

    static void carFinalPrice() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {
            new Question("How much it cost to build?"),
        };

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float cost = questions[0].answer;
        Float distributionCost = (float) ((28.0 + 100.0)/100.0);
        Float fees = (float) ((45.0 + 100.0)/100.0);

        Float finalPrice = cost * distributionCost * fees;
        
        System.out.println(String.format("Final price: %.2f", finalPrice));  

        scanner.close();
    }
}
