import java.util.Scanner;

public class Question8 {
    public static void main(String[] args) {
        sellerSalary();
    }

    static void sellerSalary() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {new Question("Base salary?"),
                                new Question("How many cars you sold?"), 
                                new Question("How much money selling those cars?"),
                                new Question("How much is the commission?"),};

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float baseSalary = questions[0].answer;
        Float numberOfCarsSold = questions[1].answer;
        Float soldAmount = questions[2].answer;
        Float comission = questions[3].answer;

        Float finalSalary = (float) (baseSalary + (numberOfCarsSold * comission) + (0.05 * soldAmount));

        System.out.println(String.format("Final Salary: %.2f", finalSalary));

        scanner.close();
    }
}
