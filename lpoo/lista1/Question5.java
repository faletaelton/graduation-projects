import java.util.Scanner;

public class Question5 {
    public static void main(String[] args) {
        elections();
    }
    
    static void elections() {
        Scanner scanner = new Scanner(System.in);
        Question[] questions = {new Question("How many voters?"),
                                new Question("How many valid votes?"), 
                                new Question("How many null votes?"),
                                new Question("How many blank votes?"),};

        for (Question element : questions) {
            System.out.println(element.sentence);
            String input = scanner.nextLine();  
            Float number = Float.parseFloat(input);
            element.answer = number;
        }
        
        Float voters = questions[0].answer;
        Float valids = questions[1].answer;
        Float nulls = questions[2].answer;
        Float blanks = questions[3].answer;

        Float validsPercentage = valids/voters * 100;
        Float nullsPercentage = nulls/voters * 100;
        Float blanksPercentage = blanks/voters * 100;
        
        System.out.println(String.format("Total voters: %.0f", voters));  
        System.out.println(String.format("valid percentage: %.1f%%", validsPercentage));  
        System.out.println(String.format("null percentage: %.1f%%", nullsPercentage));  
        System.out.println(String.format("blank percentage: %.1f%%", blanksPercentage));  

        scanner.close();
    }
}
