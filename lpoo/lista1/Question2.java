import java.util.Scanner;

class Question2 {
    public static void main(String[] args) {
        getAntecessor();
    }

    static void getAntecessor() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a integer to get its predecessor");

        String input = scanner.nextLine();  
        int number = Integer.parseInt(input);
        int predecessor = number -  1;
        
        System.out.println(String.format("The predecessor is: %d", predecessor));  

        scanner.close();
    }
}