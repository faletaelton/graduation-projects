import java.util.ArrayList;
import java.util.Scanner;

public class Question4 {
    public static void main(String[] args) {
        ageInDays();
    }

    static void ageInDays() {
        Scanner scanner = new Scanner(System.in);
        String[] questions = {"How old are you?", 
                              "How many months since your last birthday?", 
                              "How many days?"};
        ArrayList<Integer> ages = new ArrayList<Integer>();

        for (String element : questions) {
            System.out.println(element);
            String input = scanner.nextLine();  
            Integer number = Integer.parseInt(input);
            ages.add(number);
        }
        
        Integer ageInDays = ages.get(0) * 365 + ages.get(1) * 30 + ages.get(2);

        System.out.println(String.format("The age in days is: %d", ageInDays));  

        scanner.close();
    }
}
