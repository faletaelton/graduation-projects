import java.util.ArrayList;
import java.util.Scanner;

class Question3 {
    public static void main(String[] args) {
        rectangleArea();
    }

    static void rectangleArea() {
        Scanner scanner = new Scanner(System.in);
        String[] dimensionNames = {"First", "Second"};
        ArrayList<Integer> dimensions = new ArrayList<Integer>();

        for (String element : dimensionNames) {
            System.out.println("Enter the " + element + " rect dimension");
            String input = scanner.nextLine();  
            Integer dimension = Integer.parseInt(input);
            dimensions.add(dimension);
        }
        
        Integer area = 1;

        for (Integer element : dimensions) {
            area = area * element;
        }

        System.out.println(String.format("The area is: %d", area));  

        scanner.close();
    }
}